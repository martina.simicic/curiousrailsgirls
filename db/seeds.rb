# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Project.create(name: 'Getting to know', description: 'Basic Getting to know', deadline: Time.now)
Project.create(name: 'Basic setup', description: 'Basic Setup of things', deadline: Time.now)
Project.create(name: 'Let\'s get to work', description: 'What are we doing', deadline: Time.now)
Project.create(name: 'What did we learn', description: 'Did we learn anything?', deadline: Time.now)

(1..10).each do |i|
  Topic.create(name: "Topic #{i}", description: "Description for the topic number #{i}")
end

(1..10).each do |i|
  Category.create(name: "Category #{i}", description: "Description for the category number #{i}")
end